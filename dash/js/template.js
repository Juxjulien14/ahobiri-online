(function($) {
  'use strict';
  $(function() {
    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('.sidebar');

    //Add active class to nav-link based on url dynamically
    //Active class can be hard coded directly in html file also as required

    //Get modal element 
      var modal = document.getElementById('simpleModal');
      //Get open modal button
      var modalBtn = document.getElementById('modalBtn');
      //Get close button
      var closeBtn = document.getElementsByClassName('closeBtn')[0];

      //Listen for open click
      modalBtn.addEventListener('click', openModal);

      //Listen for close click
      closeBtn.addEventListener('click', closeModal);

      //Listen for Outside click
      window.addEventListener('click', outsideClick);

      //Function to open modal
      function openModal(){
          modal.style.display = 'block';
      }

      //Function to close modal
      function closeModal(){
          modal.style.display = 'none';
      }

      //Function to close modal if outside click
      function outsideClick(e){
          if(e.target == modal){
          modal.style.display = 'none';
          }
      }

    function addActiveClass(element) {
      if (current === "") {
        //for root url
        if (element.attr('href').indexOf("index.html") !== -1) {
          element.parents('.nav-item').last().addClass('active');
          if (element.parents('.sub-menu').length) {
            element.closest('.collapse').addClass('show');
            element.addClass('active');
          }
        }
      } else {
        //for other url
        if (element.attr('href').indexOf(current) !== -1) {
          element.parents('.nav-item').last().addClass('active');
          if (element.parents('.sub-menu').length) {
            element.closest('.collapse').addClass('show');
            element.addClass('active');
          }
          if (element.parents('.submenu-item').length) {
            element.addClass('active');
          }
        }
      }
    }

    var current = location.pathname.split("/").slice(-1)[0].replace(/^\/|\/$/g, '');
    $('.nav li a', sidebar).each(function() {
      var $this = $(this);
      addActiveClass($this);
    })

    //Close other submenu in sidebar on opening any

    sidebar.on('show.bs.collapse', '.collapse', function() {
      sidebar.find('.collapse.show').collapse('hide');
    });


    //Change sidebar

    $('[data-toggle="minimize"]').on("click", function() {
      body.toggleClass('sidebar-icon-only');
    });

    //checkbox and radios
    $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');
  });
})(jQuery);