<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Ahobiri Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../vendors/base/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../../css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../../images/favicon.png" />
</head>

<body>
  <div class="container-scroller">
    
      <?php require_once "../../partials/_navbar1.php" ?> 

    <div class="container-fluid page-body-wrapper">
     
        <?php require_once "../../partials/_sidebar1.php" ?> 

      <div class="main-panel">          
        <div class="content-wrapper">
          <div class="row">
           <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Products</h4>
                  <p class="card-description">
                    <a href="#" id="modalBtn">Add Product</a>
                  </p>
                  <div class="table-responsive pt-3">
                    <table class="table table-dark">
                      <thead>
                        <tr>
                          <th>
                            #
                          </th>
                          <th>
                            Product name
                          </th>
                          <th>
                            Category
                          </th>
                          <th>
                            Added time
                          </th>
                          <th>
                            Price
                          </th>
                          <th>
                            Quantity
                          </th>
                          <th>
                            Option
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            1
                          </td>
                          <td>
                            Watches 
                          </td>
                          <td>
                            Electronic
                          </td>
                          <td>
                            Feb 14<sup>th </sup>2021
                          </td>
                          <td>
                            $ 14.5 each
                          </td>
                           <td>
                            14
                          </td>
                          <td>
                            <a href="#" id="modalBtn"><i class="mdi mdi-border-color btn btn-success"></i></a>
                            <a href="#"><i class="mdi mdi-close btn btn-danger"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                           2
                          </td>
                          <td>
                            Rings
                          </td>
                          <td>
                            Jewelry
                          </td>
                          <td>
                            Sept 20<sup>th</sup> 2021
                          </td>
                           <td>
                            $ 2.3 each
                          </td>
                          <td>
                            34
                          </td>
                          <td>
                            <a href="#"><i class="mdi mdi-border-color btn btn-success"></i></a>
                            <a href="#"><i class="mdi mdi-close btn btn-danger"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            3
                          </td>
                          <td>
                            Watches 
                          </td>
                          <td>
                            Electronic
                          </td>
                          <td>
                            Feb 14<sup>th</sup> 2021
                          </td>
                          <td>
                            $ 14.5 each
                          </td>
                           <td>
                            14
                          </td>
                          <td>
                            <a href="#"><i class="mdi mdi-border-color btn btn-success"></i></a>
                            <a href="#"><i class="mdi mdi-close btn btn-danger"></i></a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            4
                          </td>
                          <td>
                            Watches 
                          </td>
                          <td>
                            Electronic
                          </td>
                          <td>
                            May 04<sup>th</sup> 2021
                          </td>
                          <td>
                            $ 14.5 each
                          </td>
                           <td>
                            14
                          </td>
                          <td>
                            <a href="#"><i class="mdi mdi-border-color btn btn-success"></i></a>
                            <a href="#"><i class="mdi mdi-close btn btn-danger"></i></a>
                          </td>
                        </tr>
  
                      </tbody>
                    </table>
                  </div>
                  
                  <!-- MODALS -->
                   <div class="newprod">
                    <div id="simpleModal" class="modal">
                        <div class="modal-content">
                                <span class="closeBtn">&times;</span>
                                <h4 style="font-weight: bold;">New Product</h4>
                           <form action="" class="">
                                    <div class="">
                                       <label for="">Add image</label>
                                        <input type="" class="form-control" style="height: 100px;width: 50%;text-align: center;" placeholder="Tap to add image"> 
                                    </div>
                                    <div class="">
                                        <label for="">Product Name</label>
                                        <input type="" class="form-control">
                                    </div>   
                                    <div class="">
                                       <label for="">Category</label>
                                        <select class="form-control">
                                            <option value="">Choose category...</option>
                                            <option value="">Electonics</option>
                                            <option value="">Clothes</option>
                                            <option value="">Foods</option>
                                        </select> 
                                    </div>   
                                    <div class="">
                                       <label for="">Description</label>
                                       <textarea name="" id="" class="form-control" ></textarea>
                                    </div>
                                    <div class="">
                                       <label for="">Price</label>
                                        <input type="" class="form-control"> 
                                    </div>
                                
                                <div class="row">
                                    <div class="col"></div>
                                    <div class="col">
                                        <button class="form-control" style="margin-top: 10px;background: #000;">Create Product</button>
                                    </div>
                                </div>

                           </form>
                        </div>
                    </div>
                </div>
                 <!-- ========ends===== --> 



                </div>
              </div>
            </div>
          </div>  
        </div>  
        
        <!-- content-wrapper ends -->
       
        <?php require_once "../../partials/_footer.php" ?> 

      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="../../vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/template.js"></script>
  <!-- endinject -->
</body>

</html>
