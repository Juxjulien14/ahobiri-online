<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Ahobiri Admin</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="../../vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../vendors/base/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- inject:css -->
  <link rel="stylesheet" href="../../css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="../../images/favicon.png" />
</head>

<body>
  <div class=" container-scroller">
    
    <?php require_once "../../partials/_navbar1.php" ?> 

    <div class="container-fluid page-body-wrapper">

    <?php require_once "../../partials/_sidebar1.php" ?> 
    
      <div class="main-panel">        
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <div class="row icons-list">
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-access-point"></i> mdi mdi-access-point
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-access-point-network"></i> mdi mdi-access-point-network
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account"></i> mdi mdi-account
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-box"></i> mdi mdi-account-box
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-box-outline"></i> mdi mdi-account-box-outline
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-card-details"></i> mdi mdi-account-card-details
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-check"></i> mdi mdi-account-check
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-circle"></i> mdi mdi-account-circle
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-convert"></i> mdi mdi-account-convert
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-key"></i> mdi mdi-account-key
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-location"></i> mdi mdi-account-location
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-minus"></i> mdi mdi-account-minus
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-multiple"></i> mdi mdi-account-multiple
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-multiple-minus"></i> mdi mdi-account-multiple-minus
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-multiple-outline"></i> mdi mdi-account-multiple-outline
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-multiple-plus"></i> mdi mdi-account-multiple-plus
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-network"></i> mdi mdi-account-network
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-off"></i> mdi mdi-account-off
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-outline"></i> mdi mdi-account-outline
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-plus"></i> mdi mdi-account-plus
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-remove"></i> mdi mdi-account-remove
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-search"></i> mdi mdi-account-search
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-settings"></i> mdi mdi-account-settings
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-settings-variant"></i> mdi mdi-account-settings-variant
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-star"></i> mdi mdi-account-star
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-account-switch"></i> mdi mdi-account-switch
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-adjust"></i> mdi mdi-adjust
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-air-conditioner"></i> mdi mdi-air-conditioner
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-airballoon"></i> mdi mdi-airballoon
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-airplane"></i> mdi mdi-airplane
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-airplane-landing"></i> mdi mdi-airplane-landing
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-airplane-off"></i> mdi mdi-airplane-off
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-airplane-takeoff"></i> mdi mdi-airplane-takeoff
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-airplay"></i> mdi mdi-airplay
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-alarm"></i> mdi mdi-alarm
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <i class="mdi mdi-alert"></i> mdi mdi-alert
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php require_once "../../partials/_footer.php" ?> 

      </div>
    </div>

  </div>
  <!-- plugins:js -->
  <script src="../../vendors/base/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/template.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
</body>

</html>
